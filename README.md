# cj-lombok

期望做成类似java 的lombok类似，简化部分模版代码的问题。当前已支持自动添加ToString方法，和给加问号的字段自动添加默认值None

基本使用，在实体类上加上@data注解即可
目录
```
src
 - macros
 	data.cj  @data注解
    
 main.cj 主方法有测试方法
 User.cj 测试的实体类
```

